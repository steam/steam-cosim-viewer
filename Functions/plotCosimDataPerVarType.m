
function plotCosimDataPerVarType(signalCollection, plotOptions, includeInLegend)

if nargin<3
    includeInLegend = true;
end

if nargin<2
    plotOptions = '-';
end

nVarTypes = length(signalCollection.varTypes);

for k = 1:nVarTypes
    varType_k = signalCollection.varTypes{k};
    subplot(nVarTypes,1,k)
    signalCollection.getSignalsCollectionOfVarType(varType_k).plot(plotOptions, includeInLegend);
    ylabel(varType_k, 'Interpreter', 'none')
    % Reset color order
    ax = gca;
    ax.ColorOrderIndex = 1;
end

% Active subplot is the first
subplot(nVarTypes,1,1)

end