classdef Signals < handle
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        timeHeader      % String
        time            % Array of doubles
        valuesHeaders   % Cell array of strings
        values          % Matrix of doubles (columns are different signals)
    end
    
    methods
        function obj = Signals(timeHeader, time, valuesHeaders, values)
            obj.timeHeader = timeHeader;
            obj.time = time;
            obj.valuesHeaders = valuesHeaders;
            obj.values = values;
        end
        %------------------------------------------------------------------
        function plot(obj, plotOptions, includeInLegend)
            for i = 1:size(obj.values,2)
                if includeInLegend
                    plot(obj.time, obj.values(:,i), plotOptions, 'DisplayName', obj.valuesHeaders{i})
                    legend('-DynamicLegend');
                    legend('show')
                    set(legend,'Interpreter','none');
                else
                    plot(obj.time, obj.values(:,i), plotOptions)
                end
                hold on                
            end
            xlabel(obj.timeHeader, 'Interpreter', 'none')
        end
        %------------------------------------------------------------------
        function varTypes = getVarTypes(obj)
            for i = 1:size(obj.values,2)
                varTypes{i} = obj.valuesHeaders{i}(1);
            end
            varTypes = unique(varTypes);
        end
        %------------------------------------------------------------------
        function selectedSignals = getSignalsOfVarType(obj, varType)
            selectedValuesHeaders = {};
            selectedValues = [];
            for i = 1:size(obj.values,2)
                if obj.valuesHeaders{i}(1) == varType
                    selectedValuesHeaders = [selectedValuesHeaders, obj.valuesHeaders{i}];
                    selectedValues = [selectedValues, obj.values(:,i)];
                end
                selectedSignals = Signals(obj.timeHeader, obj.time, selectedValuesHeaders, selectedValues);
            end
        end
        %------------------------------------------------------------------
        function concatSignal = concat(obj, signal1)
            if isempty(obj.valuesHeaders)
                concatSignal = signal1;
            else
                concatTime = [obj.time; signal1.time];
                concatValues = [obj.values; signal1.values];
                concatSignal = Signals(obj.timeHeader, concatTime, obj.valuesHeaders, concatValues);
            end
        end
        %------------------------------------------------------------------
    end
end


