classdef SignalsCollectionParser < handle
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods(Static)
        function obj = SignalsCollectionParser()
        end
        %------------------------------------------------------------------
        function signalsCollection = fromFile(filePath)
            
            f = fopen(filePath);
            fileLines = textscan(f,'%s','delimiter','\n');
            fileLines = fileLines{1};
            fclose(f);
            
            emptyRows = find(strcmp(fileLines, ''));
            separatorsRows = unique([0; emptyRows; length(fileLines)]);
            nSignals = length(separatorsRows)-1;
            
            for k=1:nSignals
                firstRow = separatorsRows(k)+1;
                lastRow = separatorsRows(k+1)-1;
                
                headers = textscan(fileLines{firstRow},'%s','delimiter',',');
                headers = headers{1};
                timeHeader = headers{1};
                valuesHeaders = headers(2:end);
                
                data = dlmread(filePath, ',', [firstRow 0 lastRow-1 length(headers)-1]);
                time = data(:,1);
                values = data(:, 2:end);
                
                signalsArray(k) = Signals(timeHeader, time, valuesHeaders, values);
            end
            signalsCollection = SignalsCollection(signalsArray);
            signalsCollection.filePath = filePath;

        end
        %------------------------------------------------------------------
    end
end