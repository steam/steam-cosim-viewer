clear
close all
clc

addpath(genpath(cd));


%% Inputs

dataDir1 = 'S:\FCC\CosTheta_v22b_38_v1\ElMaTh\COMSOL_PSpice\4\Output1\1_PSPICE\Input';
dataDir2 = 'S:\FCC\CosTheta_v22b_38_v1\ElMaTh\COMSOL_PSpice\4\Output1\1_PSPICE\Output';

iterOffset = 0; % Zero (default) or positive to shift the iterations of cosimData2 with respect to cosimData1
flagDisplayIterations = true; % True (default) or false, if false displays only concatenated final data
startingTimeWindow = 0; % Zero (defalut) based index, to start the plot from a given time-window


%% Import data

cosimData1 = parseCosimData(dataDir1);
cosimData2 = parseCosimData(dataDir2);


%% Plots

if flagDisplayIterations

    nTimeWindows = min(length(cosimData1), length(cosimData2));
    previousSignalsCollection1 = [];
    previousSignalsCollection2 = [];

    for i = (startingTimeWindow + 1) : nTimeWindows % For every time-window
        nIterations = min(length(cosimData1{i}), length(cosimData2{i}));

        for j=1:nIterations  % For every iteration of the given time-window

            % Figure with signalsCollection1
            figure('units','normalized','outerposition',[0 0 0.5 1])
            signalsCollection1 = cosimData1{i}{j};
            plotCosimDataPerVarType(signalsCollection1)
            title(signalsCollection1.filePath, 'Interpreter', 'none');
            % Previous signalCollection1 (if present)
            if ~isempty(previousSignalsCollection1)
                plotCosimDataPerVarType(previousSignalsCollection1, ':', false)
            end
            
            % Apply offset to cosimData2
            if (j+iterOffset<=nIterations)
                outerIter2 = i;
                innerIter2 = j+iterOffset;
            else
                if (i+1<=nTimeWindows)
                    outerIter2 = i+1;
                    innerIter2 = 1;
                else
                    break
                end                    
            end

            % Figure with signalsCollection2
            figure('units','normalized','outerposition',[0.5 0 0.5 1])
            signalsCollection2 = cosimData2{outerIter2}{innerIter2};
            plotCosimDataPerVarType(signalsCollection2)
            title(signalsCollection2.filePath, 'Interpreter', 'none');
            % Previous signalCollection2 (if present)
            if ~isempty(previousSignalsCollection2)
                plotCosimDataPerVarType(previousSignalsCollection2, ':', false)
            end

            pause
            close all

            previousSignalsCollection1 = signalsCollection1;
            previousSignalsCollection2 = signalsCollection2;
        end
    end

end


%% Concatenate and plot

% Concatenate cosimData1
concatSignalsCollection1 = SignalsCollection([]);
for i=1:length(cosimData1) % For every time-window
    concatSignalsCollection1 = concatSignalsCollection1.concat(cosimData1{i}{end});
end
concatSignalsCollection1.filePath = dataDir1;
% Plot concatenated cosimData1
figure('units','normalized','outerposition',[0 0 0.5 1])
plotCosimDataPerVarType(concatSignalsCollection1)
title(['Concatenated signals from ', concatSignalsCollection1.filePath], 'Interpreter', 'none');

% Concatenate cosimData2
concatSignalsCollection2 = SignalsCollection([]);
for i=1:length(cosimData2) % For every time-window
    concatSignalsCollection2 = concatSignalsCollection2.concat(cosimData2{i}{end});
end
concatSignalsCollection2.filePath = dataDir2;
% Plot concatenated cosimData2
figure('units','normalized','outerposition',[0.5 0 0.5 1])
plotCosimDataPerVarType(concatSignalsCollection2)
title(['Concatenated signals from ', concatSignalsCollection2.filePath], 'Interpreter', 'none');

