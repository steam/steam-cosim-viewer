
function cosimData = parseCosimData(dataDir)

    fileList = dir(dataDir);

    % Read cosimulation data into a matrix of cells (some of them may be empty)
    for k = 1:length(fileList)

        if ~fileList(k).isdir
            indices = regexp(fileList(k).name, '\d+', 'match');
            if length(indices)==2
                outerIndex = str2double(indices{1})+1;
                innerIndex = str2double(indices{2})+1;

                dataAsMatrix{outerIndex, innerIndex} = SignalsCollectionParser.fromFile([dataDir, '\', fileList(k).name]);
            end
        end
    end

    % Filter empty cells
    for k=1:size(dataAsMatrix,1)
        dataRow = dataAsMatrix(k,:);
        emptyCells = cellfun(@isempty,dataRow);
        cosimData{k} = dataRow(~emptyCells);
    end

end