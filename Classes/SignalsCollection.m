classdef SignalsCollection < handle
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        signalsArray    % Array of Signals
        varTypes        % Cell array of characters ('V', 'I', 'R', ...)
        filePath        % Path to the original input file (if any)
    end
    
    methods
        function obj = SignalsCollection(signalsArray)
            obj.signalsArray = signalsArray;
            obj.defineVarTypes();
        end
        %------------------------------------------------------------------
        function plot(obj, plotOptions, includeInLegend)
            for i = 1:length(obj.signalsArray)
                obj.signalsArray(i).plot(plotOptions, includeInLegend);
            end
            legend(gca,'show');
        end
        %------------------------------------------------------------------
        function defineVarTypes(obj)
            obj.varTypes = {};
            for i = 1:length(obj.signalsArray)
                obj.varTypes = [obj.varTypes, obj.signalsArray(i).getVarTypes()];
            end
            obj.varTypes = unique(obj.varTypes);
        end
        %------------------------------------------------------------------
        function selectedSignalsCollection = getSignalsCollectionOfVarType(obj, varType)
            selectedSignalsArray = [];
            for i = 1:length(obj.signalsArray)
                selectedSignals = obj.signalsArray(i).getSignalsOfVarType(varType);
                if ~isempty(selectedSignals.valuesHeaders)
                    selectedSignalsArray = [selectedSignalsArray, selectedSignals];
                end
                selectedSignalsCollection = SignalsCollection(selectedSignalsArray);
            end
        end
        %------------------------------------------------------------------
        function concatSignalsCollection = concat(obj, signalsCollection1)
            if isempty(obj.signalsArray)
                concatSignalsCollection = signalsCollection1;
            else
                for i = 1:length(obj.signalsArray)
                    concatSignalsArray(i) = obj.signalsArray(i).concat(signalsCollection1.signalsArray(i));
                end
                concatSignalsCollection = SignalsCollection(concatSignalsArray);
            end
        end
        %------------------------------------------------------------------ 
    end
end

    
    